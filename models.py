import datetime

from django.db import models


class Poll(models.Model):
    title = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')

    def was_recently_published(self):
        return self.pub_date >= datetime.datetime.now() - datetime.timedelta(days=1)

    was_recently_published.admin_order_field = 'pub_date'
    was_recently_published.boolean = True
    was_recently_published.short_description = 'Opublikowane ostatnio?'

    def __str__(self):
        return self.title


class Question(models.Model):
    poll = models.ForeignKey(Poll)
    question = models.CharField(max_length=200)
    number = models.PositiveIntegerField(default=0)

    def __str__(self):
        return self.question


class Choice(models.Model):
    ques = models.ForeignKey(Question)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)
    next_question = models.ForeignKey(Question, null=True, blank=True, related_name='Choice+')

    def __str__(self):
        return self.choice_text

# Create your models here.
