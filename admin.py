from django.contrib import admin

from pierwsza.models import Poll, Question, Choice


class Questionline(admin.TabularInline):
    model = Question
    extras = 3


class PollAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['title']}),
        ('Publication date', {'fields': ['pub_date']})
    ]
    inlines = [Questionline]

    list_display = ('title', 'pub_date')
    list_filter = ['pub_date']


class Choiceinline(admin.TabularInline):
    model = Choice
    fk_name = 'ques'
    extras = 3


class QuestionAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['question']}),
        ('Question number', {'fields': ['number']})
    ]
    inlines = [Choiceinline]

    list_display = ('question', 'poll', 'number')
    ordering = ['poll', 'number']


admin.site.register(Question, QuestionAdmin)
admin.site.register(Poll, PollAdmin)
# Register your models here.
