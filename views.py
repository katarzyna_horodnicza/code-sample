# Create your views here.
from __future__ import unicode_literals
import io

from django.shortcuts import render, redirect
import matplotlib

matplotlib.use('SVG')

from pylab import *

rcParams['figure.figsize'] = 5, 2

from pierwsza.models import Poll, Question, Choice


def index(request):
    # mam dwie listy- jedna liste wszystkich polli, druga pusta liste
    poll_list = list(Poll.objects.all().order_by("pub_date"))
    poll_list_zakon = []
    new_list = []

    zakonczono = request.session.get('zapisane_id', [])

    # dla kazdej ankiety w liscie ankiet, spawdz czy ona zostala wypelniona czy nie

    for poll in poll_list:
        # sprawdzam czy jego poll_id mam w tej sesji czy nie mam
        if str(poll.id) in zakonczono:
            poll_list_zakon.append(poll)
        else:
            new_list.append(poll)

    return render(request, "index.html", {'poll_list_zakon': poll_list_zakon,
                                          'new_list': new_list})



def question_view(request, poll_id, question_id):
    question_id = int(question_id)

    if request.method == 'GET':

        return get_question_view(request, question_id, poll_id)

    elif request.method == 'POST':
        return seleted_choice_view(request, poll_id, question_id)


def get_question_view(request, question_id, poll_id):
    # jesli wybor zostal zapisany- to chce go odtworzyc,
    # jesli nie byl zapisany to zwraca mi pustę listę

    zapisany_wybor = request.session.get('selected_list', [])
    zapisane_id = [int(wybor_id) for wybor_id in zapisany_wybor]

    id_list = request.session.get('zapisane_id', [])
    question = Question.objects.get(id=question_id)
    choice_list = list(Choice.objects.filter(ques=question))

    return render(request, "question.html", {'question': question,
                                             'poll_id': poll_id,
                                             'choice_list': choice_list,
                                             'zapisane_id': zapisane_id,
                                             'id_list': id_list})


def missing_choice_view(request, question_id, poll_id):
    question = Question.objects.get(id=question_id)
    choice_list = Choice.objects.filter(ques=question_id)

    return render(request, "question.html", {'question': question,
                                             'poll_id': poll_id,
                                             'choice_list': choice_list,
                                             'bad_choice': True})


def going_to_next_question(request, poll_id, question_id, selected_choice):
    # przycisk zapisz
    question = Question.objects.get(id=question_id)  # uzyskuję wszystkie pytania po id
    question_list = list(Question.objects.filter(poll=poll_id).order_by(
        'number'))  # uzyskuję liste wszystkich pytan po numerze pytania posort

    current_question_index = question_list.index(question)  # uzyskuję indeks obecnego pytania w liscie
    if selected_choice.next_question is None:  # jeśli wybor nie ma uzupelnionego pola next question to:
        return no_selected_choice(request, current_question_index, question_list, poll_id)

    else:
        # jesli wybór ma uzupelnione nextquestion to:
        next_question = selected_choice.next_question

        return redirect('/pierwsza/' + poll_id + '/' + str(next_question.id))


def get_previous_choice_id(request, question_id):
    list_choice_objects = list(Choice.objects.filter(ques=question_id))
    list_choice = [choice.id for choice in list_choice_objects]  # lista id choiców do danego pytania

    list_of_choice_id = request.session.get('selected_list', [])
    zapisane_choice_id = [int(wybor_id) for wybor_id in list_of_choice_id]  # lista id choiców zapisanych w sesji

    for session_id in list_choice:  # dla każdego id w liście id choiców do danego pytania
        if session_id in zapisane_choice_id:  # jeśli id jest na liscie id choiców zapisanych w sesji
            return session_id

    return None


def save_to_session(request, selected_choice):
    selected_choice_list = request.session.get('selected_list', [])
    selected_choice_list.append(selected_choice.id)
    request.session['selected_list'] = selected_choice_list


def add_vote(request, selected_choice):
    selected_choice.votes += 1
    selected_choice.save()
    save_to_session(request, selected_choice)


def finding_previous_choice(request):
    list_of_choice_id = request.session.get('selected_list', [])
    zapisane_choice_id = [int(wybor_id) for wybor_id in list_of_choice_id]
    previous_choice_id = zapisane_choice_id[-1]
    return previous_choice_id


def remove_from_session(request, choice_id):
    selected_choice_list = request.session.get('selected_list', [])
    selected_choice_list.remove(choice_id)
    request.session['selected_list'] = selected_choice_list


def decrese_vote(request, previous_choice_id):
    previous_choice = Choice.objects.get(id=previous_choice_id)

    previous_choice.votes -= 1
    previous_choice.save()
    remove_from_session(request, previous_choice_id)


def update_vote(request, selected_choice, question_id):
    previous_choice_id = get_previous_choice_id(request, question_id)
    decrese_vote(request, previous_choice_id)
    add_vote(request, selected_choice)


def saving_votes(selected_choice_id, request, question_id, poll_id):
    selected_choice = Choice.objects.get(id=selected_choice_id)
    previous_choice = get_previous_choice_id(request, question_id)

    if previous_choice == None:
        add_vote(request, selected_choice)
    elif previous_choice == int(selected_choice_id):
        pass
    else:
        update_vote(request, selected_choice, question_id)

    return going_to_next_question(request, poll_id, question_id, selected_choice)


def seleted_choice_view(request, poll_id, question_id):
    selected_choice_id = request.POST.get("choice_id", None)

    if selected_choice_id is not None:

        return saving_votes(selected_choice_id, request, question_id, poll_id)

    elif selected_choice_id is None:

        return missing_choice_view(request, question_id, poll_id)


def no_selected_choice(request, current_question_index, question_list, poll_id):
    if current_question_index < len(question_list) - 1:
        next_question = question_list[current_question_index + 1]
        # przejdz do kolejnego pytania
        return redirect('/pierwsza/' + poll_id + '/' + str(next_question.id))
    else:
        # koncze ankiete
        # chce zeby to cookie zachowalo mi informacje, ktora ankieta zostala zakonczona po jej id

        id_list = request.session.get('zapisane_id', [])
        id_list.append(poll_id)
        request.session['zapisane_id'] = id_list

        return redirect('/pierwsza/last_page.html')


def last_page(request):
    information = "To już ostatnie pytanie, dziekujemy za wypelnienie ankiety!"

    back = "Wróć do strony głównej"

    return render(request, "last_page.html", {'information': information,
                                              'back': back})


def default_poll_view(request, poll_id):
    # widok ma kierowac na pytanie pierwsze w ankiecie,
    # ktore ma jakis id, ale w danej ankiecie jest ustawione jako pierwsze

    # wyciagam pytanie pierwsze z danej ankiety
    questions = Question.objects.filter(poll=poll_id).order_by('number')
    first_question = questions[0]

    return question_view(request, poll_id, first_question.id)


def results_view(request):
    poll_id_list = request.session.get('zapisane_id', [])
    id_ankiety = poll_id_list[-1]
    last_poll_list = list(Poll.objects.filter(id=id_ankiety))
    last_poll = last_poll_list[0]
    title = last_poll.title
]

    for poll_id in poll_id_list:
        question_1 = Question.objects.filter(poll=poll_id).order_by('number')
        for ques in question_1:
            choices = list(Choice.objects.filter(ques=ques))
            ques.choice_list = choices

            axis_choices = list(Choice.objects.filter(ques=ques))  # lista wyborów dla wykresów

            vote_list = []  # lista glosow oddanych na dany choice do danego pytania
            for choice in choices:
                vote_list.append(choice.votes)

            percent_list = []
            for vote in vote_list:
                percent = int(round(vote / sum(vote_list) * 100))
                percent_list.append(percent)
            ques.percent_list = percent_list

            vote_list.reverse()
            axis_choices.reverse()

            matplotlib.rc('font', family='Arial')

            y_pos = np.arange(len(axis_choices)) + 0.5
            x_pos = vote_list
            plt.barh(y_pos, x_pos, xerr=y_pos, align='center', height=0.5, alpha=0.5, color='b')
            plt.yticks(y_pos, axis_choices)

            imgdata = io.StringIO()
            plt.savefig(imgdata)
            plt.clf()
            buf = imgdata.getvalue()
            ques.svg_graph = buf
            imgdata.close()

    return render(request, 'results.html', {'question_1': question_1,
                                            'title': title, })
